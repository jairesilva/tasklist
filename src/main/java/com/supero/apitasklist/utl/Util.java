package com.supero.apitasklist.utl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.google.common.base.Splitter;

public class Util {
	
	public static Map<String, String> splitToMap(String in) {
        return Splitter.on(",").withKeyValueSeparator("=").split(in);
	}
	
	public static java.sql.Date stringToDate(String data) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date parsed = formato.parse(data);
		return new java.sql.Date(parsed.getTime());
	}
	
	public static String dateToString(Date data) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		return formato.format(data);
	}
	
	
	public static Date formataTada(Date data) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		return formato.parse(data.toString());
	}
	
}
