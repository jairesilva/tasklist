package com.supero.apitasklist.message;

public enum Status {
	SUCESSO("200", "OK operação realizada com sucesso!"),
	ERRO("400", "Ocorreu algum problema!");
	
	private String codigo;
    private String mensagem;
    
    private Status(String codigo, String msg) {
        this.codigo = codigo;
        this.mensagem = msg;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getMensagem() {
        return mensagem;
    }
    
}
