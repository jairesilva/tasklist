package com.supero.apitasklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTasklistApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTasklistApplication.class, args);
	}

}
