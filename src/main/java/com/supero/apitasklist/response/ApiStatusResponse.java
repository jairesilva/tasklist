package com.supero.apitasklist.response;

public class ApiStatusResponse {

	private int status;
	private String message;
	private Object object;

	public ApiStatusResponse(int status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public ApiStatusResponse(int status, String message, Object object) {
		this.status = status;
		this.message = message;
		this.object = object;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
