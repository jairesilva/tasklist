package com.supero.apitasklist.service;


import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.supero.apitasklist.Cache;
import com.supero.apitasklist.model.Task;
import com.supero.apitasklist.repository.TaskRepository;

@Service
public class TaskService {

	@Autowired
	private TaskRepository repository;

	private final Logger LOG = LoggerFactory.getLogger(TaskService.class);

	@Cacheable(Cache.TASK)
	public boolean addTask(String titulo, String descricao)
			throws ParseException {
		boolean parametrosInvalidos = Strings.isNullOrEmpty(titulo) && Objects.isNull(descricao);
		
		Task retorno = null;
		if (!parametrosInvalidos) {
			Task task = new Task();
			task.setTitulo(titulo);
			task.setDescricao(descricao);
			task.setStatus(1);
			task.setDataCriacao(java.sql.Date.valueOf(LocalDate.now()));

			retorno = repository.save(task);
		}
		return (retorno != null) ? true : false;
	}
	
	public List<Task> findAll() {

		Iterable<Task> it = repository.findAll();

		ArrayList<Task> tasks = new ArrayList<Task>();
		it.forEach(e -> tasks.add(e));

		return tasks;
	}
	
	public boolean updateTask(String taskToModify) throws ParseException, JSONException {
		JSONObject taskToModifyJson = new JSONObject(taskToModify);
		
		Task taskBank = repository.findById(taskToModifyJson.optInt("id"));
		if(taskBank.getId() == taskToModifyJson.optInt("id")) {
			
			taskBank.setTitulo(taskToModifyJson.optString("titulo"));
			taskBank.setDescricao(taskToModifyJson.optString("descricao"));
			taskBank.setStatus(Integer.parseInt(taskToModifyJson.optString("status")));
			
			/*
			 *  Status:
			 *  1 = nova
			 *  2 = concluida
			 *  3 = removida
			 */
			if(taskToModifyJson.optString("status").equals("2")){
				taskBank.setDataConclusao(java.sql.Date.valueOf(LocalDate.now()));
			}else if (taskToModifyJson.optString("status").equals("3")) {
				taskBank.setDataRemocao(java.sql.Date.valueOf(LocalDate.now()));
			}
			taskBank.setDataEdicao(java.sql.Date.valueOf(LocalDate.now()));
			taskBank.setId(taskBank.getId());
			
			return (repository.save(taskBank) != null) ? true : false;
		}else {
			return false;
		}
	}
	
	public boolean deleteTask(Integer id) {
		Task task = repository.findById(id);
		if(task == null || task.getId() != id){
			return false;
		}else{
			repository.delete(task);
			return true;
		}
	}
	
	@CacheEvict(cacheNames = { Cache.TASK })
	public void evictCache() {
		LOG.info("Evict cache {}", Cache.TASK);
	}
	
}