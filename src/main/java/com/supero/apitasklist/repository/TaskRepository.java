package com.supero.apitasklist.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.supero.apitasklist.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, String>  {

    @SuppressWarnings("unchecked")
	Task save(Task persisted);
    
    Task findById(Integer id);
    
    void delete(Task entity);

}
