package com.supero.apitasklist.controller;

import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.supero.apitasklist.message.Status;
import com.supero.apitasklist.response.ApiStatusResponse;
import com.supero.apitasklist.service.TaskService;

@RestController
@RequestMapping(path = "/api-tasklist/v1")
public class TaskListController {

	@Autowired
	private TaskService service;

	private final Logger LOG = LoggerFactory.getLogger(TaskListController.class);

	@ApiOperation(value = "Add task")
	@RequestMapping(path = "/task", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApiStatusResponse addTask(@RequestParam("titulo") String titulo, @RequestParam("descricao") String descricao) {

		ApiStatusResponse response;
		try {
			if (service.addTask(titulo, descricao)) {
				response = new ApiStatusResponse(Integer.valueOf(Status.SUCESSO.getCodigo()),
						Status.SUCESSO.getMensagem(), HttpStatus.OK);
			} else {
				response = new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
						HttpStatus.EXPECTATION_FAILED);
			}
		} catch (Exception e) {
			LOG.error("", e);
			service.evictCache();
			response = new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
					HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	@ApiOperation(value = "List tasks")
	@RequestMapping(path = "/task", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApiStatusResponse getAllTask() {

		try {
			return new ApiStatusResponse(Integer.valueOf(Status.SUCESSO.getCodigo()), Status.SUCESSO.getMensagem(),
					service.findAll());

		} catch (Exception e) {
			LOG.error("", e);
			service.evictCache();
			return new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
					HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@ApiOperation(value = "Update task")
	@RequestMapping(path = "/task", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApiStatusResponse updateTask(@RequestParam("modifiedTask") String modifiedTask) {

		try {
			if(service.updateTask(modifiedTask)) {
				return new ApiStatusResponse(Integer.valueOf(Status.SUCESSO.getCodigo()), Status.SUCESSO.getMensagem());
			}else{
				return new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
						HttpStatus.EXPECTATION_FAILED);
			}

		} catch (Exception e) {
			LOG.error("", e);
			service.evictCache();
			return new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
					HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@ApiOperation(value = "Delete task")
	@RequestMapping(path = "/task", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApiStatusResponse deleteTask(@RequestParam("id") String id) {

		try {
			if(service.deleteTask(Integer.valueOf(id))) {
				return new ApiStatusResponse(Integer.valueOf(Status.SUCESSO.getCodigo()), Status.SUCESSO.getMensagem());
			}else{
				return new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
						HttpStatus.EXPECTATION_FAILED);
			}

		} catch (Exception e) {
			LOG.error("", e);
			service.evictCache();
			return new ApiStatusResponse(Integer.valueOf(Status.ERRO.getCodigo()), Status.ERRO.getMensagem(),
					HttpStatus.EXPECTATION_FAILED);
		}
	}
	
}
