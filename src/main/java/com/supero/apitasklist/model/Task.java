package com.supero.apitasklist.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Task {

	@JsonProperty("id")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@JsonProperty("titulo")
	@Column(name = "titulo_tarefa")
	private String titulo;

	@JsonProperty("status")
	@Column(name = "nr_status")
	private Integer status;

	@JsonProperty("descricao")
	@Column(name = "descricao")
	private String descricao;
	
	@JsonProperty("dataCriacao")
	@Column(name = "dt_criacao")
	private Date dataCriacao;
	
	@JsonProperty("dataEdicao")
	@Column(name = "dt_edicao")
	private Date dataEdicao;
	
	@JsonProperty("dataRemocao")
	@Column(name = "dt_remocao")
	private Date dataRemocao;

	@JsonProperty("dataConclusao")
	@Column(name = "dt_conclusao")
	private Date dataConclusao;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataEdicao() {
		return dataEdicao;
	}

	public void setDataEdicao(Date dataEdicao) {
		this.dataEdicao = dataEdicao;
	}

	public Date getDataRemocao() {
		return dataRemocao;
	}

	public void setDataRemocao(Date dataRemocao) {
		this.dataRemocao = dataRemocao;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

}